# coding: utf8

fournitures_scolaires = [
    {'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11, 'Quantité': 8},
    {'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120, 'Quantité': 1},
    {'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2, 'Quantité': 1, 'Quantité': 1},
    {'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4, 'Quantité': 1},
    {'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6, 'Quantité': 1},
    {'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3, 'Quantité': 1},
    {'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8, 'Quantité': 3},
    {'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9, 'Quantité': 1},
    {'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25, 'Quantité': 1},
    {'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13, 'Quantité': 1}

]

poids_maximal = 4


class bcolors:
    vert = '\33[92m'
    jaune = '\33[93m'
    rouge = '\33[91m'
    bleu = '\33[94m'
    rose = '\33[95m'

    reset = '\033[0m'  # réinitialise la couleur
    g = '\033[1m'  # gras
    s = '\033[4m'  # surligner
    i = '\033[3m'  # italique


def tri(fournitures, key=lambda x: x, reverse=False):
    """
    Fonction de tri universelle, reproduisant le comportement de la méthode list.sort() de la bibliotheque standard.
    Entrées:
        fournitures: liste contenant les données à trier
        key: fonction (lamdba ou non) qui permet de personnaliser le tri
        reverse: booleen determinant si on trie du plus grand ou du plus petit
    Sorties:
        fournitures: liste triée
    """
    for i in range(len(fournitures) - 1):
        mini = i
        for j in range(i + 1, len(fournitures)):
            if key(fournitures[j]) < key(fournitures[mini]):
                mini = j
        fournitures[i], fournitures[mini] = fournitures[mini], fournitures[i]
    return fournitures[::-1] if reverse else fournitures


def remplissage(fournitures, max_poids, priorite=None):
    """
    Cette fonction remplit la malle
    Entrées:
        fournitures: liste de dictionnaires représentants les objets disponibles
        max_poids: le poids maximum que la malle peut contenir
        priorite: permet de definir un ordre de priorité (en gros ça détermine avec quelle paramètre on va trier la malle (aucune, le max de poids, ou en fonction de la mana)
    Sorties:
        mis_dans_malle: liste de dictionnaires réprésentants les objets mis dans la malle
    """
    if priorite is not None:
        fournitures = tri(fournitures, key=priorite, reverse=True)

    mis_dans_malle = []
    for f in fournitures:
        poids_total = 0
        while f['Poids'] * f['Quantité']+ poids_total >= max_poids:
            while f['Quantité'] > 1:
                f['Quantité'] -= 1
        for i in mis_dans_malle:
            # on additionne au poids total le poids de chacun des objet de la liste mis_dans_malle
            poids_total += i["Poids"] * f["Quantité"]
        if f['Poids'] * f['Quantité']+ poids_total < max_poids:
            # on ajoute à la liste mis_dans_malle les éléments de la liste de dictionnaire fournitures (pas tous, on les ajoutes en fonction du tri qu'on a choisi et au max_poids)
            # à condition qu'elle respecte la condition ci-dessus
            mis_dans_malle.append(f)

    return mis_dans_malle


"BONUS: méthode par force brute"


def liste_binaire(nb_fournitures):
    liste = []
    for i in range(2 ** nb_fournitures):
        nb_binaire = bin(i)[2:]
        nb_binaire = (nb_fournitures - len(nb_binaire)) * '0' + nb_binaire
        liste.append(nb_binaire)
    return liste


def liste_combinaisons(fournitures):
    binaires = liste_binaire(len(fournitures))
    combinaisons = []
    for nb_binaire in binaires:
        combinaison = []
        for i, bit in enumerate(nb_binaire):
            if bit == '1':
                combinaison.append(fournitures[i])
        combinaisons.append(combinaison)
    return combinaisons


def calcul_mana(combinaison):
    mana = 0
    for objet in combinaison:
        mana += objet['Mana']
    return mana

def calcul_poids(combinaison):
    poids = 0
    for objet in combinaison:
        poids += objet['Poids']
    return poids

def calcul_quantitee(combinaison):
    quantitee = 0
    for objet in combinaison:
        quantitee += objet['Quantité']
    return quantitee

def meilleur_choix(liste_choix, poids_max):
    mana_max = 0
    for choix in liste_choix:
        mana = calcul_mana(choix)
        poids = calcul_poids(choix)
        quantité = calcul_quantitee(choix)
        if mana >= mana_max and poids * quantité <= poids_max:
            mana_max = mana
            top_choix = choix
    return top_choix


def malle(fournitures):
    """
    On affiche tout ce que l'on met dans la malle
    Entrées:
        fournitures: liste de dictionnaires
    """

    poids = 0
    mana = 0
    for i, l in enumerate(fournitures):
        print(f"---{bcolors.bleu}Objet n°{bcolors.rouge}{i + 1}{bcolors.reset}---")
        # on affiche le nom de l'objet
        print(f"{bcolors.vert}Nom{bcolors.reset} : {bcolors.g}{l['Nom']}{bcolors.reset}")
        # on affiche le poids de l'objet
        print(f"{bcolors.rouge}Poids{bcolors.reset} : {bcolors.g}{l['Poids']}{bcolors.reset}")
        # on affiche la mana de l'objet
        print(f"{bcolors.jaune}Mana{bcolors.reset} : {bcolors.g}{l['Mana']}{bcolors.reset}")
        # on affiche la quantité de l'objet
        print(f"{bcolors.rose}Quantité{bcolors.reset} : {bcolors.g}{l['Quantité']}{bcolors.reset}")
        print()
        poids += l["Poids"] * l["Quantité"]
        mana += l["Mana"] * l["Quantité"]

    print(f"{bcolors.g}Poids total : {poids} kg.{bcolors.reset}")
    print(f"{bcolors.g}Mana total : {mana}.{bcolors.reset}")
    print(f"{bcolors.g}Rapport mana / poids : {mana / poids}.{bcolors.reset}")


'''----------------------------------------------------IHM--------------------------------------'''
a = 1
while a == 1:
    print(f"{bcolors.bleu}{bcolors.g}{bcolors.i}{bcolors.s}Bienvenue dans notre algo de malle.{bcolors.reset}")

    le_poids = input("Entrez le poids maximal (laisser blanc pour défaut) : ")
    poids_maximal = int(le_poids) if le_poids != '' else poids_maximal

    print(f"{bcolors.vert}[a]{bcolors.reset} {bcolors.g}{bcolors.s}Remplir n'importe comment{bcolors.reset}")
    print(
        f"{bcolors.rouge}[b]{bcolors.reset} {bcolors.g}{bcolors.s}Remplir pour le plus lourd possible{bcolors.reset}")
    print(
        f"{bcolors.jaune}[c]{bcolors.reset} {bcolors.g}{bcolors.s}Remplir pour le plus de mana possible{bcolors.reset}")
    print(
        f"{bcolors.bleu}[d]{bcolors.reset} {bcolors.g}{bcolors.s}Remplir avec le meilleur rapport mana / poids{bcolors.reset}")
    print(
        f"{bcolors.rose}[f]{bcolors.reset} {bcolors.g}{bcolors.s}Remplir grâce à une méthode par force brûte{bcolors.reset}")

    mode = input("Entrez le mode de remplissage : ")
    # tant que la réponse entrée ne vaut pas 'a', 'b' ou 'c' on demande de réentrer une valeur(a, b ou c)
    while mode.lower() not in ["a", "b", "c", "d", "f"]:
        print(f"{bcolors.g}{bcolors.rouge}ERREUR{bcolors.reset}")
        print("Veuillez entrer une valeur valide (a/b/c/d/f)")
        mode = input("Entrez le mode de remplissage : ")

    if mode.lower() == 'a':
        print(f"{bcolors.vert}Vous avez choisi de trier la malle n'importe comment")
        # si on entre 'a' on lance la fonction malle qui a pour argument la fonction remplissage
        malle(remplissage(fournitures_scolaires, poids_maximal))
    elif mode.lower() == 'b':
        print(f"{bcolors.rouge}Vous avez choisi de trier la malle la plus lourde possible")
        # si on entre 'b' ça fait pareille qu'en haut sauf que cette fois on a l'argument priorite qui vas personnaliser le mode de trie en fonction du poids
        malle(remplissage(fournitures_scolaires, poids_maximal, priorite=lambda x: x["Poids"]))
    elif mode.lower() == 'c':
        print(f"{bcolors.jaune}Vous avez choisi de trier la malle avec le plus de mana possible")
        # pareil qu'en haut sauf que cette fois l'argument priorite vas personnaliser le mode de trie en fonction de la mana
        malle(remplissage(fournitures_scolaires, poids_maximal, priorite=lambda x: x["Mana"]))
        print()
    elif mode.lower() == 'd':
        print(f"{bcolors.bleu}Vous avez choisi de trier la malle avec le meilleur rapport mana / poids")
        # pareil qu'en haut sauf que l'argument priorite vas personnaliser le mode de trie pour fournir le meilleur rapport mana / poids
        malle(remplissage(fournitures_scolaires, poids_maximal, priorite=lambda x: (x["Mana"] / x["Poids"])))
    else:
        # on lance la fonction malle qui a pour argument la fonction 'meilleur choix' qui elle même a pour arguments les fournitures scolaire et le poids max
        # en gros la fonction malle a pour argument la liste de dico 'meilleur choix
        print(f"{bcolors.rose}Vous avez choisi de trier la malle grâce à une méthode par force brûte")
        malle(meilleur_choix(liste_combinaisons(fournitures_scolaires), poids_maximal))

    print()
    print(f"{bcolors.bleu}{bcolors.g}voulez vous continuer de remplir la malle?{bcolors.reset}")
    reponse = input(f"{bcolors.jaune}[y]{bcolors.reset} ou {bcolors.jaune}[n]{bcolors.reset} : ")
    while reponse.lower() not in ["y", "n"]:
        print(f"{bcolors.g}{bcolors.rouge}ERREUR{bcolors.reset}")
        print("Veuillez entrer une valeur valide (y / n)")
        print("voulez vous continuer de remplir la malle?")
        reponse = input("y / n : ")
    if reponse.lower() == 'y':
        pass
    else:
        print(f"{bcolors.rouge}A la prochaine!{bcolors.reset}")
        a += 1