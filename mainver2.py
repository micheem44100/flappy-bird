from random import randint
import csv

# Importation du fichier characters.csv
characters = [1]
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    characters = f.readlines()


def partie():
    score = 0

    passage = randint(1, 3)

    flappy = randint(1, 3)

    while flappy:
        score = score + 1

        passage = randint(1, 3)

        flappy = randint(1, 3)

        if score == 4:
            score = 50 + score

    return score


print(f"la partie est fini est le score est {partie()}")