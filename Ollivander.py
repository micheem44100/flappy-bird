def ollivander(somme_a_rendre):
    ''' Entrée (entier) : une somme d'argent à rendre
        Sortie (string) : phrase indiquant les billets à utiliser
    '''
    noises, mornilles, gallions = somme_a_rendre
    rendu = f'\nPour {noises} noises, {mornilles} mornilles, {gallions} gallions:\n'

    mornilles = mornilles + noises // 
    noises %= 29

    gallions = gallions + mornilles // 17
    mornilles %= 17

    print(rendu)
    print(f"Il faut rendre {gallions} gallion(s), {mornilles} mornille(s) et {noises} noise(s)")
    return somme_a_rendre
