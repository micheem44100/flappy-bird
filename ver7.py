from random import randint
import csv

from random import randint
import csv

# Importation du fichier characters.csv
Characters = []
with open("characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        Characters.append(dico)

#codage de la partie aléatoire
def partie ():
    poteau = randint(1,3)
    flappy = randint(1,3)
    score =  0

    while flappy == poteau :


        score +=1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 50

    return score
print(f"Le score de la partie aléatoire est {partie()}.")



#codage de l'IMH
def partie3():
    i = Characters[0]
    poteau = randint(1,3)
    flappy = randint(1,3)
    score = 0
    n = 0

    while flappy == poteau:
        score += 1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 46
            break
    return score

for n in range(11):
    for i in range (140):
        print(f"Le score de la partie n°{n} de {Characters[i]['Name']} est de {partie3()}")


