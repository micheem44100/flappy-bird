
from random import randint

# Importation du fichier characters.csv
Characters = []
with open("characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        Characters.append(dico)

#codage de la partie aléatoire
def partie ():
    poteau = randint(1,3)
    flappy = randint(1,3)
    score =  0

    while flappy == poteau :


        score +=1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 50

    return score
print(f"Le score de la partie aléatoire est {partie()}.")



#codage de l'IMH
def partie3():
    poteau = randint(1,3)
    flappy = randint(1,3)
    score = 0

    while flappy == poteau:
        score += 1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 46
            break
    return score

somme = 0

for nb_joueur in range(140):
    nb_score = 0
    for nb_partie in range(10):
        somme += partie3()
        nb_score += partie3()
    Characters[nb_joueur]['score'] = nb_score

for nb_joueur in range(140):
    print(f"Le classement est donc {Characters[nb_joueur]['score']} pour {Characters[nb_joueur]['Name']} ")
    
moyenne = somme / nb_joueur
print(f"La moyenne est de {moyenne}")

