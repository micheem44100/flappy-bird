
from random import randint
# Importation du fichier characters.csv
Characters = []
with open("characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        Characters.append(dico)

#codage de la partie aléatoire
def partie ():
    poteau = randint(1,3)
    flappy = randint(1,3)
    score =  0

    while flappy == poteau :


        score +=1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 50

    return score
print(f"Le score de la partie aléatoire est {partie()}.")


def partie3():
    poteau = randint(1,3)
    flappy = randint(1,3)
    score = 0

    while flappy == poteau:
        score += 1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 46
            break
    return score

somme = 0
classement = {}
for i in range (140):
    for n in range(11):
        somme += partie3()
        noms = Characters[i]['Name']
        points = partie3()
        
        print(f"Le score de la partie n°{n} de {Characters[i]['Name']} est de {partie3()}")
        classement[noms] = points
        classement_trier = sorted(classement.items(), key=lambda x: x[1])


moyenne = somme / i
print(f"La moyenne est de {moyenne}")
print(f"m {classement_trier}")


