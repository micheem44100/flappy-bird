from random import randint
import csv

from random import randint
import csv

# Importation du fichier characters.csv
Characters = []
with open("characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        Characters.append(dico)

#codage de la partie aléatoire
def partie ():
    poteau = randint(1,3)
    flappy = randint(1,3)
    score =  0

    while flappy == poteau :


        score +=1
        poteau = randint(1,3)
        flappy = randint(1,3)

        if score == 4:
            score += 50

    return score
print(f"Le score de la partie aléatoire est {partie()}.")


#codage de la partie des 140 persos d'Harry Potter
def partie2():
    i = Characters[0]
    poteau = randint(1,3)
    flappy = randint(1,3)
    score = 0
    for i in range(1, 141):
        i += 1
        while flappy == poteau:
            score += 1
            poteau = randint(1,3)
            flappy = randint(1,3)

            if score == 4:
                score += 50

                break
    return score
for i in range (141):
    print(f"Le score de {Characters[i]['Name']} est de {partie2()}")




