from random import randint
'''Importation du fichier characters.csv'''
Characters = []                                                    #créations d'une liste pour stocker le fichier charactes.csv
with open("characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        Characters.append(dico)

'''Codage de la partie aléatoire'''
def partie():
    poteau = randint(1, 3)                                          #On donne une valeur aléatoire comprise entre 1 et 3 au poteau(point de passage)
    flappy = randint(1, 3)                                          #On donne une valeur aléatoire comprise entre 1 et 3 à notre joueur
    score = 0

    while flappy == poteau:                                        #Tant que notre joueur est dans notre point de passage on applique les instructions du while
        score += 1                                                 #Instructions du while: -On ajoute 1 au score
        poteau = randint(1, 3)                                                             #-On donne une valeur à notre poteau
        flappy = randint(1, 3)                                                             #-On donne une valeur à notre joueur

        if score == 4:
            score += 46                                            #On ajoute 46 au score si sa valeur atteint 4
            break                                                  #On boucle le while si le score atteint 50
    return score                                                   #On renvoie la variable score


'''codage du classement après les 140 parties'''
totale = 0                                                         #On crée une variable totale qui va stocker tous les scores de tous les personnages pour toutes les parties
point_perso = 0                                                    #On crée une variable points perso qui va stocker tous les score accumulé à chaque partie par personnages
for personnage in range(140):
    point_perso = 0
    for n in range(10):
        point_perso += partie()
        totale += partie()
    Characters[personnage]['point'] = point_perso

Characters = sorted(Characters, key=lambda k: k['point'], reverse=True) #On trie le dictionnaire Characters qui contient les noms en clé et en valeur les points

for dic_perso in Characters:
    print(dic_perso['Name'], dic_perso['point'])

moyenne = totale / personnage                                           #On fait une moyenne des résultats en divisant la variable somme par i

'''Codage de l'IHM'''

IHM = input(f"Que voulez vous faire? : ")
if IHM == "Afficher la partie aléatoire":
    print(f"Le score de la partie aléatoire est de {partie()}")

elif IHM == "Afficher la moyenne":
    print(f"La moyenne est de {moyenne}")
